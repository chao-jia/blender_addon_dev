bl_info = {
    "name": "Simple",
    "description": "",
    "version": (0, 0, 1),
    "blender": (2, 80, 0),
    "warning": "", # used for warning icon and text in addons panel
}

import bpy, os
import datetime


def info(context):
    os.system('cls')
    print('info ({})'.format(datetime.datetime.now()))
    for ob in context.scene.objects:
        print(ob)


class SIMPLE_OT_simple_operator(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "simple.simple_operator"
    bl_label = "Print Objects"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        info(context)
        return {'FINISHED'}


class SIMPLE_PT_simple_panel(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_idname = "SIMPLE_PT_simple_panel"
    bl_label = "Simple Panel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Simple panel"
    bl_context = "objectmode" # not "object"

    def draw(self, context):
        layout = self.layout

        obj = context.object

        row = layout.row()
        row.label(text="Hello world!", icon='WORLD_DATA')
        if obj != None:
            row = layout.row()
            row.label(text="Active object is: " + obj.name)
            row = layout.row()
            row.prop(obj, "name")

        row = layout.row()
        row.operator("mesh.primitive_cube_add")
        row = layout.row()
        row.operator("simple.simple_operator")
        

classes = (SIMPLE_OT_simple_operator, SIMPLE_PT_simple_panel)

register, unregister = bpy.utils.register_classes_factory(classes)

if __name__ == "__main__":
    register()
#    unregister()
#    if bpy.ops.simple.simple_operator.poll():
#        bpy.ops.simple.simple_operator()