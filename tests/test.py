import bpy, bmesh
import os, time, functools

def back_to_object_mode(obj):
    bpy.context.view_layer.objects.active = obj
    for window in bpy.context.window_manager.windows:
        screen = window.screen
        for area in screen.areas:
            print("area.type: ", area.type)
            if area.type == 'VIEW_3D':
                override = {'window': window, 'screen': screen, 'area': area}
                break

    print(override)
    me = obj.data
    # bpy.ops.object.mode_set(override, mode = 'EDIT') # crash, bug? https://developer.blender.org/T62074
    
    bm = bmesh.from_edit_mesh(me)
    bm.verts[2].hide_set(False)
    bmesh.update_edit_mesh(me)
    
    # bpy.ops.object.mode_set(override, mode = 'OBJECT')

def set_active_obj(name):
    for obj in bpy.context.scene.objects:
            if not obj.hide_get():
                print (obj.name)
                if obj.name == name:
                    bpy.context.view_layer.objects.active = obj
                    break

def toggle_visibility(name):
    set_active_obj(name)
    obj = bpy.context.view_layer.objects.active
    me = obj.data
    # bm = bmesh.new()
    bpy.ops.object.mode_set(mode = 'EDIT')
    bm = bmesh.from_edit_mesh(me)

    bm.verts.ensure_lookup_table()
    print(bm.verts[2].co)
    print ('-*-*-*-*-*-*-*-*-*')
    bm.verts[2].hide_set(not bm.verts[2].hide)
    bmesh.update_edit_mesh(me)
    # bpy.app.timers.register(functools.partial(back_to_object_mode, obj), first_interval=1)


os.system("cls")
print("list(D.objects): ", list(bpy.data.objects))
bpy.context.view_layer.objects.active = bpy.data.objects['Cube']
print("C.active_object.name", bpy.context.active_object.name) # read only
print("C.view_layer.objects.active.name", bpy.context.view_layer.objects.active.name)
toggle_visibility('box_points')
# bpy.ops.object.mode_set(mode='OBJECT')
bpy.app.timers.register(functools.partial(set_active_obj, 'Cube'), first_interval=1)