# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

ttm_point_glob_vert = '''
    uniform mat4 ViewProjectionMatrix;
    uniform mat4 ModelMatrix;
    uniform float point_size;
    uniform float gray_scale;
    uniform int color_mode;

    uniform float inv_num_labels;
    
    in vec3 position;
    in float red;
    in float green;
    in float blue;
    in float label_red;
    in float label_green;
    in float label_blue;
    in float odds_all;
    out vec4 vcolor;

    void main()
    {
        gl_Position = ViewProjectionMatrix * ModelMatrix * vec4(position, 1.0f);
        gl_PointSize = point_size;
        
        // color mode: texture, label, grayscale
        vec4 colors[3];
        colors[0] = vec4(red, green, blue, 1.0); 
        colors[1] = vec4(label_red, label_green, label_blue, 1.0);
        colors[2] = vec4(vec3(1.0) * odds_all * inv_num_labels * gray_scale, 1.0);

        // colors[2] = vec4(1.0) * odds_all * inv_num_labels * gray_scale;
        
        vcolor = colors[color_mode];
    }
'''

ttm_point_glob_frag = '''
    in vec4 vcolor;
    out vec4 fcolor;

    void main()
    {
        vec2 dist = gl_PointCoord - vec2(0.5);
        if (dot(dist, dist) > 0.25)
            discard;
        fcolor = vcolor;
    }
'''

ttm_point_label_vert = '''
    uniform mat4 ViewProjectionMatrix;
    uniform mat4 ModelMatrix;
    uniform float point_size;
    uniform float gray_scale;
    uniform int color_mode;

    uniform float inv_label_odds_max;
    uniform vec3 label_color;
    
    in vec3 position;
    in float red;
    in float green;
    in float blue;
    in float label_odds;
    out vec4 vcolor;

    void main()
    {
        gl_Position = ViewProjectionMatrix * ModelMatrix * vec4(position, 1.0f);
        gl_PointSize = point_size;
        float a = step(0.0001, label_odds);
        float prob = a * label_odds * inv_label_odds_max;
        float s = prob * gray_scale;
        // color mode: texture, label, grayscale
        vec4 colors[3];
        colors[0] = vec4(red, green, blue, s); 
        colors[1] = s * vec4(label_color, 1.0);
        colors[2] = vec4(s * vec3(1.0), a);

        //colors[2] = s * vec4(1.0);

        vcolor = colors[color_mode];
    }
'''

ttm_point_label_frag = '''
    in vec4 vcolor;
    out vec4 fcolor;

    void main()
    {
        vec2 dist = gl_PointCoord - vec2(0.5);
        if (dot(dist, dist) > 0.25)
            discard;
        fcolor = vcolor;
    }
'''
