import bpy, bpy_extras
import atexit
from . import ttm_import_ply

# ------------------------------------------------------------------------
#    Operators
# ------------------------------------------------------------------------

def ttm_check_selection(context):
    if TTM_ui_resource_manager.ttm_selected_obj_name != "" and context.object.name != TTM_ui_resource_manager.ttm_selected_obj_name:
        if bpy.data.objects[TTM_ui_resource_manager.ttm_selected_obj_name].hide_get(): 
            print ("unhiding {}".format(TTM_ui_resource_manager.ttm_selected_obj_name))
            bpy.data.objects[TTM_ui_resource_manager.ttm_selected_obj_name].hide_set(False)

class TTM_OT_toggle_visibility(bpy.types.Operator):
    bl_label = "Toggle Visibility"
    bl_idname = "ttm.toggle_visibility"
    bl_description = "toggle visibility"

    @classmethod
    def poll(cls,context):
        return context and context.object and context.object.ttm_options.is_tritium_ply

    def execute(self, context):
        context.object.hide_set(not context.object.hide_get())
        return {'FINISHED'}

class TTM_OT_print(bpy.types.Operator):
    bl_label = "Print"
    bl_idname = "ttm.print"
    bl_description = "print ply info"
    @classmethod
    def poll(cls,context):
        return context and context.object and context.object.ttm_options.is_tritium_ply

    def execute(self, context):
        mesh = context.object.data
        print("vertices co: ", [tuple(v.co) for v in mesh.vertices.values()])
        print("vertices color layers: ", len(mesh.vertex_colors.values()))
        if mesh.vertex_colors:
            print("vertices color: ", [d for color_layer in mesh.vertex_colors.values() for d in color_layer.data])
        print("custom layer keys: ", mesh.vertex_layers_float.keys())
        for k0 in mesh.vertex_layers_float.keys():
            print("layer: {} : {}".format(k0, ["{:.2f}".format(f.value) for f in mesh.vertex_layers_float[k0].data]))
        return {'FINISHED'}

class TTM_OT_load_ply(bpy.types.Operator, bpy_extras.io_utils.ImportHelper):
    bl_label = "Load Ply"
    bl_idname = "ttm.load_ply"
    bl_description = "Load PLY file"

    filepath: bpy.props.StringProperty(
        name="Path", 
        default="", 
        description="select a ply file", 
        subtype='FILE_PATH', 
    )

    filename_ext = ".ply"
    filter_glob: bpy.props.StringProperty(default="*.ply", options={'HIDDEN'}, )
    
    def execute(self, context):
        ttm_info = ttm_import_ply.load_ply(self.filepath)
        if ttm_info is not None:
            ttm_check_selection(context)
            TTM_ui_resource_manager.ttm_selected_obj_name = context.object.name
            options = context.object.ttm_options
            labels = context.object.ttm_labels
            extra_props = context.object.ttm_extra_props
            options.ply_path = self.filepath
            options.is_tritium_ply = True
            for i in range(ttm_info['num_labels']):
                label = labels.add()
                label.name = ttm_info['label_name'][i].decode('ascii')
                label.index = ttm_info['label_index'][i]
                label.num_points = ttm_info['num_points_in_label'][i]
                label.color = tuple(ttm_info['label_color'][i])
                label.odds_max = ttm_info['odds_max'][i]

            for prop in ttm_info['ttm_extra_props']:
                extra_prop = extra_props.add()
                extra_prop.name = prop.decode('ascii')

            # print the values to the console
            print("color_src:", options.color_src)
            print("is_tritium_ply:", options.is_tritium_ply)

        return {'FINISHED'}

# ------------------------------------------------------------------------
#    Menus
# ------------------------------------------------------------------------
def menu_func_import(self, context):
    self.layout.operator(TTM_OT_load_ply.bl_idname, text="tritium ply")


# ------------------------------------------------------------------------
#    Panel in Object Mode
# ------------------------------------------------------------------------

class TTM_PT_options_panel(bpy.types.Panel):
    bl_label = "Tritium Options"
    bl_idname = "TTM_PT_options_panel"
    bl_space_type = "VIEW_3D"   
    bl_region_type = "UI"
    bl_category = "tritium ply"
    bl_context = "objectmode"

    @classmethod
    def poll(cls,context):
        return context 

    def draw(self, context):
        layout = self.layout
        if context.object and context.object.ttm_options:
            options = context.object.ttm_options
            obj = context.object
            ttm_check_selection(context)

            # obj.type in  [‘MESH’, ...]
            # https://docs.blender.org/api/current/bpy.types.Object.html#bpy.types.Object.type
            if  options.is_tritium_ply:
                TTM_ui_resource_manager.ttm_selected_obj_name = obj.name
                mesh = obj.data
                num_visible_points = total_points = len(mesh.vertices)
                if options.color_src not in ['default', 'all_labels']:
                    num_visible_points = obj.ttm_labels.values()[int(options.color_src_selected)].num_points
                row = layout.row()
                row.label(text=obj.name)
                row = layout.row()
                row.label(text='{} / {} Pts'.format(num_visible_points, total_points))
                layout.operator("ttm.toggle_visibility", text="Show" if obj.hide_get() else "Hide")
                layout.prop(options, "point_size")
                layout.prop(options, "color_mode", text="mode")
                layout.prop(options, "gray_scale")
                layout.prop(options, "color_src", text="color")
                
                layout.separator()
                layout.operator("ttm.print")
                
            layout.separator()


class TTM_ui_resource_manager:
    addon_keymaps = []
    ttm_selected_obj_name = '' # workaround, context.scene. property cannot be altered in draw()
    @classmethod
    def clean_up(cls):
        print ("cleaning up TTM_ui_resource_manager...")
        del cls.addon_keymaps[:]
    
    @classmethod
    def register(cls):
        bpy.types.TOPBAR_MT_file_import.append(menu_func_import)
        atexit.register(cls.clean_up)

        # handle the keymap
        wm = bpy.context.window_manager
        km = wm.keyconfigs.addon.keymaps.new(name='Object Mode', space_type='EMPTY')
        km.keymap_items.new(TTM_OT_toggle_visibility.bl_idname, 'V', 'PRESS', ctrl=False, shift=False)
        cls.addon_keymaps.append(km)

    @classmethod
    def unregister(cls):
        bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)