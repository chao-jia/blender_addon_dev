import atexit
import bpy
import bgl, gpu
from gpu_extras import batch
import numpy as np
from . import ttm_shaders
from . import ttm_ui

def odds_layer_name(label):
    return 'odds_' + str(label.index)

class TTM_Renderer:
    meshes = {}
    shaders = {
        "point_glob" : gpu.types.GPUShader(ttm_shaders.ttm_point_glob_vert, ttm_shaders.ttm_point_glob_frag),
        "point_label" : gpu.types.GPUShader(ttm_shaders.ttm_point_label_vert, ttm_shaders.ttm_point_label_frag),
    }
    color_channels = ["red", "green", "blue", "label_red", "label_green", "label_blue"]

    @classmethod
    def clean_up(cls):
        print ("cleaning up TTM_Renderer...")
        cls.meshes.clear()
        cls.shaders.clear()

    @classmethod
    def register(cls):
        bpy.types.SpaceView3D.draw_handler_add(cls.render, (), 'WINDOW', 'POST_VIEW')
        atexit.register(cls.clean_up)

    @classmethod
    def unregister(cls):
        # bpy.types.SpaceView3D.draw_handler_remove(cls.render, 'WINDOW') # should be unnecessary, throws everytime
        pass

    @classmethod
    def update_meshes(cls):
        # clean up
        names_obj_in_scene = [obj.name for obj in bpy.data.objects]
        meshes_to_remove = []
        for name in cls.meshes.keys():
            if name not in names_obj_in_scene:
                print ("removing object from cache: {}".format(name))
                meshes_to_remove.append(name)
        for name in meshes_to_remove:
            del cls.meshes[name]
        if ttm_ui.TTM_ui_resource_manager.ttm_selected_obj_name not in names_obj_in_scene:
            ttm_ui.TTM_ui_resource_manager.ttm_selected_obj_name = ""
        
        # add new obj
        for obj in bpy.context.scene.objects:
            if obj.type not in ['MESH']:
                continue
            mesh = obj.data
            options = obj.ttm_options
            labels = obj.ttm_labels
            extra_props = obj.ttm_extra_props
            if not options:
                continue
            if not options.is_tritium_ply:
                continue
            if obj.name in cls.meshes.keys():
                continue
            
            print ("adding object to cache: {}".format(obj.name))
            positions = np.empty((len(mesh.vertices), 3), dtype=np.float32)
            mesh.vertices.foreach_get("co", np.reshape(positions, len(mesh.vertices) * 3))
            colors = {}
            batches = {}
            labels_odds = {}
            extra_props_for_render = {
                "odds_all": np.empty((len(mesh.vertices), ), dtype=np.float32)
            }
            for key in extra_props_for_render.keys():
                if key not in extra_props:
                    print ("Warning: property {} does not exist in ttm_ply, zeroing out".format(key))
                    extra_props_for_render[key].fill(0)
                else:
                    mesh.vertex_layers_float[key].data.foreach_get("value", extra_props_for_render[key])

                for i in range(6):
                    colors[i] = np.empty((len(mesh.vertices), ), dtype=np.float32)
                    mesh.vertex_layers_float[cls.color_channels[i]].data.foreach_get("value", colors[i])

                batches['all_labels'] = batch.batch_for_shader(
                    cls.shaders["point_glob"], 
                    'POINTS', 
                    {
                        "position": positions, 
                        "red": colors[0], 
                        "green": colors[1], 
                        "blue": colors[2],
                        "label_red": colors[3], 
                        "label_green": colors[4], 
                        "label_blue": colors[5],
                        "odds_all": extra_props_for_render["odds_all"],
                    }
                )
            
            for i, label in enumerate(labels.values()):
                layer_name = odds_layer_name(label)
                labels_odds[i] = np.empty((len(mesh.vertices), ), dtype=np.float32)
                mesh.vertex_layers_float[layer_name].data.foreach_get("value", labels_odds[i])
                print(
                    "label {0:2}: name = {1:>9}, index = {2:3}, odds_max = {3:.2f}, #pts = {4:5}, color = {5}".format
                    (i, label.name, label.index, label.odds_max, label.num_points, tuple(label.color))
                )
                batches[str(i)] = batch.batch_for_shader(
                    cls.shaders["point_label"], 
                    'POINTS', 
                    {
                        "position": positions,
                        "red": colors[0], 
                        "green": colors[1], 
                        "blue": colors[2], 
                        "label_odds": labels_odds[i], 
                    }
                )

            cls.meshes[obj.name] = {
                "batches": batches,
            }

    @classmethod
    def render(cls):
        TTM_Renderer.update_meshes()

        bgl.glEnable(bgl.GL_PROGRAM_POINT_SIZE)
        bgl.glEnable(bgl.GL_DEPTH_TEST)
        bgl.glEnable(bgl.GL_BLEND)

        for obj in bpy.data.objects:
            if obj.type in ['MESH']:
                options = obj.ttm_options
                if options and options.is_tritium_ply:
                    shader_key = "point_label"
                    if options.color_src_selected == 'all_labels':
                        shader_key = "point_glob"

                    cls.shaders[shader_key].bind()
                    cls.shaders[shader_key].uniform_float("ModelMatrix", obj.matrix_world)
                    cls.shaders[shader_key].uniform_float("ViewProjectionMatrix", bpy.context.region_data.perspective_matrix)
                    cls.shaders[shader_key].uniform_float("point_size", options.point_size)
                    cls.shaders[shader_key].uniform_float("gray_scale", options.gray_scale * 0.1)
                    cls.shaders[shader_key].uniform_int("color_mode", int(options.color_mode_selected))

                    if shader_key == "point_label":
                        label = obj.ttm_labels.values()[int(options.color_src_selected)]
                        cls.shaders[shader_key].uniform_float("inv_label_odds_max", 1.0 / label.odds_max)
                        cls.shaders[shader_key].uniform_float("label_color", label.color)
                        bgl.glDisable(bgl.GL_DEPTH_TEST)
                    elif shader_key == "point_glob":
                         cls.shaders[shader_key].uniform_float("inv_num_labels", 1.0 / len(obj.ttm_labels.values()))

                    cls.meshes[obj.name]["batches"][options.color_src_selected].draw(cls.shaders[shader_key])

        bgl.glDisable(bgl.GL_BLEND)
        bgl.glDisable(bgl.GL_DEPTH_TEST)
        bgl.glDisable(bgl.GL_PROGRAM_POINT_SIZE)
        