import bpy

# ------------------------------------------------------------------------
#    Object and Scene Properties
# ------------------------------------------------------------------------

def enum_color_src_callback(self, context):
    items=[('all_labels', "all labels", ""),]

    obj = context.object
    if obj.ttm_options.is_tritium_ply and obj.ttm_labels is not None:
        for i, label in enumerate(obj.ttm_labels.values()):
            items.append((str(i), label.name, ""))
    return items

# seems when ttm_obj is deselected, enum `color_src` is reset to '', therefore use another string property to store the latest selection
def update_color_src_callback(self, context):
    if self.color_src != '':
        self.color_src_selected = self.color_src
def update_color_mode_callback(self, context):
    if self.color_mode != '':
        self.color_mode_selected = self.color_mode

class TTM_PG_extra_prop(bpy.types.PropertyGroup):
    @classmethod
    def register(cls):
        bpy.types.Object.ttm_extra_props = bpy.props.CollectionProperty(type=TTM_PG_extra_prop)
    
    @classmethod
    def unregister(cls):
        del bpy.types.Object.ttm_extra_props

    name: bpy.props.StringProperty(
        name = "Property Name",
        default = ""
    )

class TTM_PG_label(bpy.types.PropertyGroup):
    @classmethod
    def register(cls):
        bpy.types.Object.ttm_labels = bpy.props.CollectionProperty(type=TTM_PG_label)
    
    @classmethod
    def unregister(cls):
        del bpy.types.Object.ttm_labels

    name: bpy.props.StringProperty(
        name = "Label Name",
        default = ""
    )
    index: bpy.props.IntProperty(
        name = "Label Index",
        default = 0
    )
    num_points: bpy.props.IntProperty(
        name = "Number of Points",
        default = 0
    )
    color: bpy.props.FloatVectorProperty(
        name = "Label Color",
        description="label color",
        default=(0.0, 0.0, 0.0), 
    )
    odds_max: bpy.props.FloatProperty(
        name = "maxium odds",
        description="maxium odds among all points",
        default=0.0, 
    )

class TTM_PG_options(bpy.types.PropertyGroup):
    @classmethod
    def register(cls):
        bpy.types.Object.ttm_options = bpy.props.PointerProperty(type=TTM_PG_options)
        
    @classmethod
    def unregister(cls):
        del bpy.types.Object.ttm_options

    is_tritium_ply: bpy.props.BoolProperty(
        name="Is Tritium Ply",
        description="indicates whether current object is tritium ply or not",
        default = False,
    )

    ply_path: bpy.props.StringProperty(
        name="PLY",
        description="PLY path",
        default="",
        maxlen=1024,
    )
    point_size: bpy.props.FloatProperty(
        name = "Point Size",
        description = "size of each point",
        default = 3.0,
        min = 0.01,
        max = 20.0
    )
    gray_scale: bpy.props.FloatProperty(
        name = "Scale",
        description = "scale texture/label/gray color in single label view",
        default = 10.0,
        min = 1.0,
        max = 100.0,
        step=1,
    )

    color_src_selected: bpy.props.StringProperty(
        name="Color Source Selected",
        description="selected color source",
        default="all_labels",
        maxlen=1024,
    )
    color_src: bpy.props.EnumProperty(
        name="Color Source:",
        description="Choose color source.",
        items=enum_color_src_callback,
        update=lambda self, context: update_color_src_callback(self, context)
    )
    color_mode_selected: bpy.props.StringProperty(
        name="Color Mode Selected",
        description="selected color mode",
        default='0',
        maxlen=1024,
    )
    color_mode: bpy.props.EnumProperty(
        name="Color Mode:",
        description="Choose color mode",
        items=[ ('0', "texture", ""), ('1', "label", ""), ('2', "grayscale", ""), ],
        update=lambda self, context: update_color_mode_callback(self, context)
    )
