bl_info = {
    "name": "tritium ply",
    "description": "",
    "author": "Chao Jia",
    "version": (0, 0, 3),
    "blender": (2, 80, 0),
    "location": "3D View > Side bar",
    "warning": "", # used for warning icon and text in addons panel
    "wiki_url": "",
    "tracker_url": "",
#    "category": "Development"
}
import bpy
from . import ttm_properties
from . import ttm_renderer
from . import ttm_ui

# ------------------------------------------------------------------------
#    Registration
# ------------------------------------------------------------------------

classes = (
    ttm_properties.TTM_PG_extra_prop,
    ttm_properties.TTM_PG_label,
    ttm_properties.TTM_PG_options,
    ttm_ui.TTM_OT_toggle_visibility,
    ttm_ui.TTM_OT_print,
    ttm_ui.TTM_OT_load_ply,
    ttm_ui.TTM_PT_options_panel,
)

cls_register, cls_unregister = bpy.utils.register_classes_factory(classes)

def register():
    cls_register()
    ttm_ui.TTM_ui_resource_manager.register()
    ttm_renderer.TTM_Renderer.register()
    

def unregister():
    ttm_renderer.TTM_Renderer.unregister()
    ttm_ui.TTM_ui_resource_manager.unregister()
    cls_unregister()

if __name__ == "__main__":
    register()

